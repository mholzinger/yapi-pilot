# YAPI-Pilot

YAPI-pilot is a Python-based toolkit designed to streamline the process of downloading videos from YouTube via the YouTube Data API and yt-dlp. This utility provides a command-line interface for fetching video information from specified YouTube channels or playlists and downloading them for offline viewing.

## Features

- Fetch video lists from specific YouTube channels or playlists.
- Download videos in a variety of formats and resolutions.
- Support for dry run to preview video lists before downloading.
- Multithreading support for faster downloads.
- Utilizes YouTube Data API for accurate and efficient data retrieval.
- Environment variable configuration for API keys and other sensitive information.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Python 3.6 or higher
- [yt-dlp](https://github.com/yt-dlp/yt-dlp#installation)
- A valid YouTube Data API key

## Installation

Clone the repository to your local machine:

```bash
git clone https://github.com/yourusername/yapi-pilot.git
cd yapi-pilot
```

Set up a virtual environment and install the required packages:

```bash
python -m venv venv
source venv/bin/activate  # On Windows use `venv\Scripts\activate`
pip install -r requirements.txt
```

## Configuration

Set your YouTube Data API key as an environment variable:

```bash
export YOUTUBE_API_KEY='your_youtube_data_api_key'
```

## Usage

To fetch and download videos from a YouTube channel:

```bash
python yapi-pilot.py --channel 'ChannelName' --download
```

To fetch and download videos from a YouTube playlist:


```bash
python yapi-pilot.py --playlist 'PlaylistID' --download
```

For a dry run (fetch video lists without downloading):

```bash
python yapi-pilot.py --channel 'ChannelName' --dry-run
```

## Contributing

Contributions are welcome! Please feel free to submit a pull request.

## Acknowledgments

Thanks to the creators and contributors of yt-dlp and the YouTube Data API for making this project possible.

