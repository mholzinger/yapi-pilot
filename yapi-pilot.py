import os
import argparse
import requests
import subprocess
import threading
from urllib.parse import urlencode

# Read the API key from an environment variable
API_KEY = os.getenv('YOUTUBE_API_KEY')  # Replace 'YOUR_API_KEY_HERE' with the environment variable
if not API_KEY:
    raise ValueError("Please set the YOUTUBE_API_KEY environment variable")

BASE_URL = 'https://www.googleapis.com/youtube/v3'

# Function to get channel ID from channel name
def get_channel_id(api_key, channel_name):
    search_url = f'{BASE_URL}/search?'
    search_params = {
        'part': 'snippet',
        'type': 'channel',
        'q': channel_name,
        'key': api_key
    }
    response = requests.get(search_url + urlencode(search_params))
    response_json = response.json()
    if response_json.get('items'):
        return response_json['items'][0]['id']['channelId']
    else:
        return None

# Function to get all video slugs from a channel ID or playlist ID
def get_videos(api_key, id, is_playlist=False):
    videos = []
    page_token = ''
    while True:
        if is_playlist:
            url = f'{BASE_URL}/playlistItems?'
            params = {
                'part': 'contentDetails',
                'playlistId': id,
                'maxResults': 50,
                'pageToken': page_token,
                'key': api_key
            }
        else:
            url = f'{BASE_URL}/search?'
            params = {
                'part': 'snippet',
                'channelId': id,
                'maxResults': 50,
                'order': 'date',
                'pageToken': page_token,
                'type': 'video',
                'key': api_key
            }
        response = requests.get(url + urlencode(params))
        response_json = response.json()
        items = response_json.get('items', [])
        for item in items:
            video_id = item['contentDetails']['videoId'] if is_playlist else item['id']['videoId']
            videos.append(video_id)
        page_token = response_json.get('nextPageToken')
        if not page_token:
            break
    return videos

# Function to execute yt-dlp for each video in a separate thread
def download_video(video_id):
    video_url = f'https://www.youtube.com/watch?v={video_id}'
    command = [
        'yt-dlp', 
        '--cookies', '~/.config/ytdl/cookies.txt', 
        '--merge-output-format', 'mkv', 
        '-o', '%(channel)s - %(upload_date)s - %(title)s.%(ext)s', 
        video_url
    ]
    subprocess.run(command)  # Run the command for each video

# Function to manage download threads
def download_videos(video_ids, dry_run):
    if dry_run:
        print("Dry run enabled. The following videos would be downloaded:")
        for video_id in video_ids:
            print(f'https://www.youtube.com/watch?v={video_id}')
    else:
        threads = []
        for video_id in video_ids:
            thread = threading.Thread(target=download_video, args=(video_id,))
            threads.append(thread)
            thread.start()

        for thread in threads:
            thread.join()  # Wait for all threads to complete

# Main function with argument parsing
def main():
    parser = argparse.ArgumentParser(description='Fetch YouTube videos from a channel or playlist and optionally download them using yt-dlp.')
    parser.add_argument('-c', '--channel', help='Channel name')
    parser.add_argument('-p', '--playlist', help='Playlist ID')
    parser.add_argument('--dry-run', action='store_true', help='Print out the video list without downloading')
    args = parser.parse_args()

    if args.channel:
        channel_id = get_channel_id(API_KEY, args.channel)
        if channel_id:
            video_ids = get_videos(API_KEY, channel_id)
            download_videos(video_ids, args.dry_run)
        else:
            print("Channel not found.")
    elif args.playlist:
        video_ids = get_videos(API_KEY, args.playlist, is_playlist=True)
        download_videos(video_ids, args.dry_run)
    else:
        print("Please provide a channel name or playlist ID.")

if __name__ == '__main__':
    main()
